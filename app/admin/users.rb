ActiveAdmin.register User do
	index do
		column :full_name
		column :email
		column :zip
    column :created_at
  	default_actions
  end

  show do |user|
  	panel "User Details" do
  		attributes_table_for user, :full_name, :email, :zip,
  															:is_vegan, :likes_desserts, 
  															:spicy_preference, :eating_habits, 
                                :created_at, :registration_page,
                                :other_allergies
  	end

  	panel "Foods" do
  		table_for(user.user_foods) do
  			column "Name" do |food|
  				food.food.name
  			end
  			column "Frequency" do |food|
  				food.frequency
  			end
  		end
  	end

  	panel "Food Types" do
  		table_for(user.user_food_types) do
  			column "Name" do |food|
  				food.food_type.name
  			end
  			column "Frequency" do |food|
  				food.frequency
  			end
  		end
  	end

  	panel "Allergies" do
  		table_for(user.allergies) do
  			column "Name" do |allergy|
  				allergy.name
  			end
  		end
  	end

  	panel "Diets" do
  		table_for(user.user_diets) do
  			column "Name" do |diet|
  				diet.diet.name
  			end
  			column "Frequency" do |diet|
          if diet.diet.is_special
            "Special"
          else
  				  diet.frequency
          end
  			end
  		end
  	end
  end

  csv do
    column :id
    column :full_name
    column :email
    column :zip
    column :remember_token
    column :created_at
    column :updated_at
    column("Do you have any food allergies") { |user| user.has_allergy }
    -> { Allergy.featured.each do |allergy|
      column("Are you allergic to") do |user|
        if user.allergies.include?(allergy)
          allergy.name
        else
          nil
        end
      end
    end }.call
    column("What are you allergic to?") { |user| user.other_allergies }
    column("Are you a vegitarian?") { |user| user.is_vegan }
    diet_proc = -> { Diet.featured.each do |diet|
      column(diet.name) do |user|
        tmp = user.user_diets.where(diet_id: diet.id).first
        tmp.frequency if tmp.present?
      end
    end }
    diet_proc.call
    -> { Diet.special.each do |s_diet|
      column("Any special diet that you are on") do |user|
        if user.diets.include?(s_diet)
          s_diet.name
        else
          nil
        end
      end
    end }.call
    -> { Food.all.each do |food|
      column(food.name) do |user|
        tmp = user.user_foods.where(food_id: food.id).first
        tmp.frequency if tmp.present?
      end
    end }.call
    -> { FoodType.all.each do |food_type|
      column(food_type.name) do |user|
        tmp = user.user_food_types.where(food_type_id: food_type.id).first
        tmp.frequency if tmp.present?
      end
    end }.call
    column("How do you like your food?") { |user| user.spicy_preference }
    column("Do you like dessert?") { |user| user.likes_desserts }
    column("Other eating habits") { |user| user.eating_habits }
    column("Last page accessed") { |user| user.registration_page-1 }
    column("Completion Status") { |user| user.registration_completed }
  end
end
