var FormValidator = {
  
  email: function (val) {
    return /^(?:\w+\.?)*\w+@(?:\w+\.)+\w+$/.test(val);
  },
  
  fname: function (val){
    return /^[a-zA-Z\ ]+$/.test(val);
  },
  
  zip: function(val){
    return /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(val);
  },
  
  validateFormData: function(){
    var isValidName = FormValidator.fname($('#subscribtionForm #name').val());
    var isValidEmail = FormValidator.email($('#subscribtionForm #email').val());
    var isValidZip = FormValidator.zip($('#subscribtionForm #zip').val());
    
    if(isValidName && isValidEmail && isValidZip){
      Initializer.mailSendAjax();
      $('#subscribtionForm .error').hide();
    } else{
      if(!isValidName){
        $('#subscribtionForm #name').siblings('.error').show();
      }else{
        $('#subscribtionForm #name').siblings('.error').hide();
      }
      if(!isValidEmail){
        $('#subscribtionForm #email').siblings('.error').show();
      }else{
        $('#subscribtionForm #email').siblings('.error').hide();
      }
      if(!isValidZip){
        $('#subscribtionForm #zip').siblings('.error').show();
      }else{
        $('#subscribtionForm #zip').siblings('.error').hide();
      }
    }
  }
};