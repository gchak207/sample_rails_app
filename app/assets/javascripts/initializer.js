var Initializer = {
    
  init : function(){
    Initializer.handleAnchorLinks();
    Initializer.initSlider();
    $('.reg_form button.btn').on('click', function(){
      FormValidator.validateFormData();
    });
  },
  
  handleAnchorLinks : function(){
    $('.nav_list, .nav-collapse').find('ul li a').on('click', function(){
      var act_link = $(this).attr('class');
      $('a.'+act_link).parent().addClass('active');
      $('a.'+act_link).parent().siblings().removeClass('active');
    });
  },
  
  initSlider : function(){
    
    //	thumbnail scroller for related shops on restaurant detail page 
		$('#member_slider').carouFredSel({
		  prev: '#prev',
			next: '#next',
			responsive: true,
			auto: false,
			width: 'auto',
			items: {
        width: 300,
        height: '100%',  //  optionally resize item-height
				visible: {
          width: 'auto',
          height: 'auto',
					min: 3,
					max: 5
				}
			},
			scroll: {
			  items: 1
			}
		});
  },
  
  mailSendAjax : function(){
    var sendGridURL = Initializer.getSendGridURL();
    $.ajax({
         url        : sendGridURL,
         type       : 'GET',
      success    : function(data){
        // alert('mail send');
      },
      error      : function(data){
        //show error
        // alert('Some error occured! Please Try Again.', data);
      },
      complete   : function(){
        alert('Thank You for resgistration!');
        $('#subscribtionForm').find('#name, #email, #zip').val('');
      }
    });
  },
  
  getSendGridURL : function(){
    var sname =  $('#subscribtionForm #name').val();
    var semail =  $('#subscribtionForm #email').val();
    var szip =  $('#subscribtionForm #zip').val();
    // var url =  'https://sendgrid.com/api/mail.send.json?api_user=ashaunak&api_key=shaunakvinsol212&to=samin@novus.com&toname=AShaunak&subject=Request for KitchenStadium invite.&text=Contact name : '+sname+' , Contact Id : '+semail+' , Zip : '+szip+': &from=info@domain.com';
    var url =  'https://sendgrid.com/api/mail.send.json?api_user=ashaunak&api_key=shaunakvinsol212&to=sa@kitchenstadium.com&toname=KitchenStadium&subject=Request for KitchenStadium invite.&text=Contact name : '+sname+' , Contact Id : '+semail+' , Zip : '+szip+': &from=info@kitchenstadium.com';
    return url
  }
  
};

$(function(){ Initializer.init(); })