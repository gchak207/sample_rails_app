class AllergiesController < ApplicationController
  def create
  	@allergy = Allergy.new(params[:allergy])
  	if @allergy.save
  		flash[:success] = "Allergy created"
  		redirect_to root_path
  	else 
  		flash[:error] = "Allergy could not be created"
  		redirect_to root_path
  	end
  end 
end
