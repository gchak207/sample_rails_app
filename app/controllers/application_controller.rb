class ApplicationController < ActionController::Base
  include ApplicationHelper
  protect_from_forgery

  before_filter :initialize_mixpanel

  private
  	def initialize_mixpanel
  		env = {
		  'REMOTE_ADDR' => request.env['REMOTE_ADDR'],
		  'HTTP_X_FORWARDED_FOR' => request.env['HTTP_X_FORWARDED_FOR'],
		  'rack.session' => request.env['rack.session'],
		  'mixpanel_events' => request.env['mixpanel_events']
		}
  		@mixpanel = Mixpanel::Tracker.new 'd498d25d1d09343f2992d8e2ef0b1222', { env: env }
  	end
end
