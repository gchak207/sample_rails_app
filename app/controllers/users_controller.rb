class UsersController < ApplicationController
  def create
  	@user = User.new(params[:user])
    @user.is_vegan = false
    @user.likes_desserts = true
    @user.spicy_preference = "medium"
    @user.registration_page = 1
  	if @user.save
      Notifier.register(@user).deliver
      subscribe(@user)
      login(@user)
  		redirect_to edit_allergies_user_path(@user)
  	else
      flash[:error] = @user.errors.full_messages
  		redirect_to root_url
  	end
  end

  def edit_allergies
  	@allergies = Allergy.featured
  	@user = User.find(params[:id])
    @step = 1
  end

  def set_allergies
    @mixpanel.track "First Page"
    @user = User.find(params[:id])
    @user.registration_page = @user.registration_page+1
    if params[:other_allergy_name].present?
      @user.other_allergies = params[:other_allergy_name]
    end
    @user.save
    @user.user_allergies.each do |allergy|
      allergy.destroy
    end
    if params[:has_allergy] == 'true'
      unless params[:allergy_ids].nil?
        params[:allergy_ids].each do |id|
          @user.allergies << Allergy.find(id)
        end
      end
    end
    redirect_to edit_diet_user_path(@user)
  end

  def edit_diet
    @user = User.find(params[:id])
    @step = 2
  end

  def set_diet
    @mixpanel.track "Second Page"
    @user = User.find(params[:id])
    @user.update_attributes(is_vegan: params[:is_vegan], registration_page: @user.registration_page+1)
    Diet.featured.each do |diet|
      frequency = params[diet.name]
      unless frequency.nil?
        if user_diet = @user.user_diets.where(diet_id: diet.id).first
          user_diet.update_attributes(frequency: frequency)
        else
          UserDiet.create(user_id: @user.id,
                          diet_id: diet.id,
                          frequency: frequency)
        end    
      end
    end
    @user.user_diets.each do |diet|
      if diet.diet.is_special == true
        diet.destroy
      end
    end
    unless params[:sp_diet_ids].nil?
      params[:sp_diet_ids].each do |id|
        @user.diets << Diet.find(id)
      end
    end
    redirect_to edit_food_user_path(@user)
  end

  def edit_food
    @user = User.find(params[:id])
    @step = 3
  end

  def set_food
    @mixpanel.track "Third Page"
    @user = User.find(params[:id])
    @user.update_attributes(registration_page: @user.registration_page+1)
    Food.all.each do |food|
      frequency = params[food.name]
      unless frequency.nil?
        if user_food = @user.user_foods.where(food_id: food.id).first
          user_food.update_attributes(frequency: frequency)
        else
          UserFood.create(user_id: params[:id],
                          food_id: food.id,
                          frequency: frequency)    
        end
      end
    end
    FoodType.all.each do |foodtype|
      frequency = params[foodtype.name]
      unless frequency.nil?
        if user_food_type = @user.user_food_types.where(food_type_id: foodtype.id).first
          user_food_type.update_attributes(frequency: frequency)
        else
          UserFoodType.create(user_id: params[:id],
                        food_type_id: foodtype.id,
                        frequency: frequency)  
        end  
      end
    end
    redirect_to edit_tastes_user_path(User.find(params[:id]))
  end

  def edit_tastes
    @user = User.find(params[:id])
    @step = 4
  end

  def set_tastes
    @mixpanel.track "Fourth Page"
    @user = User.find(params[:id])
    @user.update_attributes(spicy_preference: params[:spicy_preference],
                            likes_desserts: params[:likes_desserts],
                            eating_habits: params[:eating_habits],
                            registration_page: @user.registration_page+1)
    # intercom_custom_data.user[:app_activated_at] = Time.now
    # intercom_custom_data.user[:spicy_preference] = params[:spicy_preference]
    # intercom_custom_data.user[:likes_desserts] = params[:likes_desserts]
    # intercom_custom_data.user[:eating_habits] = params[:eating_habits]
    # intercom_custom_data.user[:registration_page] = @user.registration_page
    flash[:success] = "Thank you for your time! We will be in touch."
    redirect_to root_url
  end

  def finish
    @user = User.find(params[:id])
  end

  def mailchimp
    @lists = Gibbon::API.lists.list({:filters => {:list_name => "Kitchen Stadium Subscribers"}})
    list_id = @lists["data"].first["id"]
    Gibbon::API.lists.subscribe({:id => list_id, :email => {:email => 'billy@hackathon.io'}, :double_optin => false})
  end

  private
    def subscribe(user)
      @lists = Gibbon::API.lists.list({:filters => {:list_name => "Kitchen Stadium Subscribers"}})
      list_id = @lists["data"].first["id"]
      name = @user.full_name.split(' ')
      first = name.shift
      last = name.join(' ')
      Gibbon::API.lists.subscribe({:id => list_id, 
                                :email => {:email => @user.email}, 
                           :merge_vars => {:FNAME => first, :LNAME => last},
                         :double_optin => false})
    end
end
