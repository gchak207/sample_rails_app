module UsersHelper
	def diet_val(user, diet, frequency)
		if user.diets.count == 0  || !(user_diet = user.user_diets.where(diet_id: diet.id).first)
		# 	if frequency == 'never'
		# 		return true
		# 	else 
		# 		return false
		# 	end
			return false
		end
		return user_diet.frequency == frequency
	end

	def food_val(user, food, frequency)
		if user.foods.count == 0 || !(user_food = user.user_foods.where(food_id: food.id).first)
		# 	if frequency == 'never'
		# 		return true
		# 	else 
				return false
		# 	end
		end
		return user_food.frequency == frequency
	end

	def food_type_val(user, food_type, frequency)
		if user.food_types.count == 0 || !(user_food_type = user.user_food_types.where(food_type_id: food_type.id).first)
		# 	if frequency == 'never'
		# 		return true
		# 	else 
		 		return false
		# 	end
		end
		return user_food_type.frequency == frequency
	end

	def spicy_val(user, val)
		return user.spicy_preference == val
	end
	def dessert_val(user, val)
		return user.likes_desserts == val
	end
end
