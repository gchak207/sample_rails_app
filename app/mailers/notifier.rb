class Notifier < ActionMailer::Base
  default from: "ks@kitchenstadium.com"

  def register(user)
    @user = user
    mail(:to => @user.email, :subject => "Welcome to Kitchen Stadium")
  end
end
