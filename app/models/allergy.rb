class Allergy < ActiveRecord::Base
  attr_accessible :name, :featured

  has_many :user_allergies
  has_many :users, through: :user_allergies

  scope :featured, where(featured: true)

end
