class Diet < ActiveRecord::Base
  attr_accessible :name, :is_vegan, :is_special, :featured
  has_many :user_diets
  has_many :users, through: :user_diets

  scope :special, where(is_special: true)
  scope :featured, where(featured: true)
end
