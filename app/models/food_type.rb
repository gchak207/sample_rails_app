class FoodType < ActiveRecord::Base
  attr_accessible :name
  has_many :user_food_types
  has_many :users, through: :user_food_types
end
