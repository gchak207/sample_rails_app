class Picture < ActiveRecord::Base
  attr_accessible :front_page, :title, :url
  scope :front_page, where(front_page: true)
end
