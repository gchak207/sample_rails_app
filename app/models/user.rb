class User < ActiveRecord::Base
  attr_accessible :email,
  						:full_name,
  						:zip,
  						:remember_token,
  						:password,
  						:password_confirmation,
              :is_vegan,
              :likes_desserts,
              :eating_habits,
              :spicy_preference,
              :registration_page,
              :other_allergies

  before_create :create_token
  before_save { |user| user.email.downcase! }

  has_many :user_allergies
  has_many :allergies, through: :user_allergies

  has_many :user_diets
  has_many :diets, through: :user_diets

  has_many :user_foods
  has_many :foods, through: :user_foods

  has_many :user_food_types
  has_many :food_types, through: :user_food_types

  validates :full_name, presence: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence:   true,
                    format:     { with: VALID_EMAIL_REGEX }

  def has_allergy
    if self.allergies.count > 0
      true
    else
      false
    end
  end

  def registration_page
    if self[:registration_page]
      return [5, self[:registration_page]].min
    else
      0
    end
  end

  def registration_completed
    if self[:registration_page].present? && self[:registration_page] > 4
      return true
    else
      return false
    end
  end

  def allergy_str
    self.allergies.map do |a|
      a.name
    end.join(", ")
  end

  def fav_foods_str
    self.user_foods.where(frequency: 'often').map do |f|
      f.food.name
    end.join(", ")
  end

  def fav_food_types_str
    self.user_food_types.where(frequency: 'often').map do |f|
      f.food_type.name
    end.join(", ")
  end

  def diets_str
    self.diets.map do |a|
      a.name
    end.join(", ")
  end

  private
  	def create_token
  		self.remember_token = SecureRandom.urlsafe_base64
    end


end
