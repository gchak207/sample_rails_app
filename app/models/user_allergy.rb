class UserAllergy < ActiveRecord::Base
  attr_accessible :allergy_id, :user_id
  belongs_to :user
  belongs_to :allergy
end
