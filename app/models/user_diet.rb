class UserDiet < ActiveRecord::Base
  attr_accessible :diet_id, :frequency, :user_id

  belongs_to :user
  belongs_to :diet
end
