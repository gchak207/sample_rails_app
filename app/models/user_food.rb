class UserFood < ActiveRecord::Base
  attr_accessible :food_id, :frequency, :user_id
  belongs_to :food
  belongs_to :user
end
