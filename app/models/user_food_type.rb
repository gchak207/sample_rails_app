class UserFoodType < ActiveRecord::Base
  attr_accessible :food_type_id, :frequency, :user_id
  belongs_to :food_type
  belongs_to :user
end
