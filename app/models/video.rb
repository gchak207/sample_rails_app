class Video < ActiveRecord::Base
  attr_accessible :front_page, :html_embed, :title

  scope :front_page, where(front_page: true)
end
