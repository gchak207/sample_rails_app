class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :full_name
      t.string :email
      t.string :zip
      t.string :password
      t.string :remember_token
      t.timestamps
    end
  end
end
