class CreateAllergies < ActiveRecord::Migration
  def change
    create_table :allergies do |t|
      t.string :name
      t.boolean :featured
      t.timestamps
    end
  end
end
