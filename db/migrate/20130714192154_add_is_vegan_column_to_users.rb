class AddIsVeganColumnToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :is_vegan, :boolean, default: false 
  end
end
