class AddVeganColumnToDiet < ActiveRecord::Migration
  def change
  	add_column :diets, :is_vegan, :boolean
  end
end
