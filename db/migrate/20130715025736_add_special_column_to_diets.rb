class AddSpecialColumnToDiets < ActiveRecord::Migration
  def change
  	add_column :diets, :is_special, :boolean
  end
end
