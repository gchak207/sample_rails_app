class AddSpicyDessertsAndFoodHabitsToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :spicy_preference, :string
  	add_column :users, :likes_desserts, :boolean
  	add_column :users, :eating_habits, :text
  end
end
