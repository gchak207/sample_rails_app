class AddFeaturedToDiet < ActiveRecord::Migration
  def change
    add_column :diets, :featured, :boolean
  end
end
