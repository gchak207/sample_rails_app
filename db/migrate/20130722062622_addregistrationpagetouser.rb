class Addregistrationpagetouser < ActiveRecord::Migration
  def change
    add_column :users, :registration_page, :integer
    add_column :users, :other_allergies, :text
  end
end
