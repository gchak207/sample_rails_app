class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.string :title
      t.text :url
      t.boolean :front_page

      t.timestamps
    end
  end
end
