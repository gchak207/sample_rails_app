class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :title
      t.text :html_embed
      t.boolean :front_page

      t.timestamps
    end
  end
end
