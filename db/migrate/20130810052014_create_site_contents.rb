class CreateSiteContents < ActiveRecord::Migration
  def change
    create_table :site_contents do |t|
      t.string :key
      t.text :content

      t.timestamps
    end
  end
end
